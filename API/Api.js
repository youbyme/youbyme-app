import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
const API_URL = "http://youbyme.nicolas-broquet.fr";
//const API_URL = "http://10.0.2.2:8000";
const SERVER_UNAVAILABLE = "Problème de connexion avec le serveur";
const TITLE_ERROR = "Erreur";

export function loginAPI(login, password) {
    return fetch(API_URL + '/v1/login_check', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            username: login,
            password: password,
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        });
}

export function getNewToken(refreshToken) {
    return fetch(API_URL + '/v1/token/refresh', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'refresh_token': refreshToken.toString()
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        });
}

export function getCurrentUser(token) {
    return GETRequest(API_URL + '/api/v1/auth/currentuser', token);
}

export function getSkillsList(token) {
    return fetch(API_URL + '/api/v1/competences',{
        method: 'GET',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code) {
                return responseJson;
            } else {
                return responseJson.sort(compareSkill);
            }
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}

export function getVotableUsersList(utilId, token) {
    return fetch(API_URL + '/api/v1/utilisateurs/votables',{
        method: 'GET',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code) {
                return responseJson;
            } else {
                return responseJson.sort(compareUser);
            }
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}

export function getUserStats(utilId,token) {
   return GETRequest(API_URL + '/api/v1/utilisateurs/' + utilId + '/progress', token);
}

export function getBadgesList(token) {
    return fetch(API_URL + '/api/v1/badges',{
        method: 'GET',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code) {
                return responseJson;
            } else {
                return responseJson.sort(compareBadge);
            }
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}

export function getAssignations(utilId, token) {
  return GETRequest(API_URL + "/api/v1/badgeassignations?utilisateur=" + utilId + "&valide=" + 1, token);
}

export function GETRequest(url, token) {
    return fetch(url, {
        method: 'GET',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}

export function getUnachievedSkillsList(utilId, token) {
    return fetch(API_URL + '/api/v1/utilisateurs/' + utilId + '/compVotables',{
        method: 'GET',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.code) {
                return responseJson;
            } else {
                return responseJson.sort(compareSkill);
            }
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}


export function makeAVote(utilisateurEmetteur, utilisateurReceveur, competence, token) {
    const jsonBody = {
        type: 0,
        utilisateurEmetteur: {
            utilId: utilisateurEmetteur
        },
        utilisateurReceveur: {
            utilId: utilisateurReceveur
        },
        competence: {
            compId: competence
        }
    };

    return fetch(API_URL + '/api/v1/votes',{
        method: 'POST',
        headers: new Headers({
            'Authorization' : 'Bearer ' + token,
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(jsonBody)
    }).then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            Alert.alert(TITLE_ERROR, SERVER_UNAVAILABLE);
        })
}

// Permet de classer les objets par ordre alphabétique
function compareSkill(a, b) {
    // Use toUpperCase() to ignore character casing
    const compNomA = a.compNom.toUpperCase();
    const compNomB = b.compNom.toUpperCase();

    let comparison = 0;
    if (compNomA > compNomB) {
        comparison = 1;
    } else if (compNomA < compNomB) {
        comparison = -1;
    }
    return comparison;
}

// Permet de classer les objets par ordre alphabétique
function compareUser(a, b) {
    // Use toUpperCase() to ignore character casing
    const utilPrenomA = a.utilPrenom.toUpperCase();
    const utilPrenomB = b.utilPrenom.toUpperCase();

    let comparison = 0;
    if (utilPrenomA > utilPrenomB) {
        comparison = 1;
    } else if (utilPrenomA < utilPrenomB) {
        comparison = -1;
    }
    return comparison;
}

// Permet de classer les objets par ordre alphabétique
function compareBadge(a, b) {
    // Use toUpperCase() to ignore character casing
    const badgNomA = a.badgNom.toUpperCase();
    const badgNomB = b.badgNom.toUpperCase();

    let comparison = 0;
    if (badgNomA > badgNomB) {
        comparison = 1;
    } else if (badgNomA < badgNomB) {
        comparison = -1;
    }
    return comparison;
}
