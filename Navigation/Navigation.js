import React from 'react';
import {StyleSheet} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import {createBottomTabNavigator, createStackNavigator, createAppContainer, createSwitchNavigator} from "react-navigation";

// Style
import theme from './../Assets/Styles/Theme';

// Screens
import Skills from '../Layout/Skills/Skills';
import Badges from '../Layout/Badges/Badges';
import Votes from '../Layout/Votes/Votes';
import VoteUser from '../Layout/Votes/VoteUser';
import VoteSkill from '../Layout/Votes/VoteSkill';
import ChooseVote from '../Layout/Votes/ChooseVote';
import Login from "../Layout/Login";

const VoteStackNavigator = createStackNavigator({
  Votes: {
    screen: Votes,
    navigationOptions: {
        header: null
    }
  },
  VoteUser: {
    screen: VoteUser,
    navigationOptions: {
        title: 'Choisir un étudiant'
    }
  },
  VoteSkill: {
    screen: VoteSkill,
    navigationOptions: {
        title: 'Choisir une compétence'
    }
  },
  ChooseVote: {
    screen: ChooseVote,
    navigationOptions: {
      title: 'Voter pour un étudiant'
    }
  }
});

const MainTabNavigator = createBottomTabNavigator({
    Skills: {
        screen: Skills,
        navigationOptions: {
            tabBarIcon: () => {
                return (
                    <FontAwesomeIcon
                        name="list-ol"
                        style = {styles.icon}
                    />
                )
            },
        }
    },
    Votes: {
        screen: VoteStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return (
                    <FontAwesomeIcon
                        name="vote-yea"
                        style = {styles.icon}
                    />

                )
            }
        }
    },
    Badges: {
        screen: Badges,
        navigationOptions: {
            tabBarIcon: () => {
                return (
                    <FontAwesomeIcon
                        name="award"
                        style = {styles.icon}
                    />
                )
            }
        }
    }
},{
    initialRouteName: 'Votes',
    tabBarOptions: {
        showLabel: false,
        showIcon: true,
        activeBackgroundColor: theme.bgTabBarActive,
        inactiveBackgroundColor: theme.bgTabBarInactive,
    }
});

// Map d'authentification
const AuthStack = createStackNavigator({
    SignIn: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    }
});

const AppNavigator = createSwitchNavigator({
    App: MainTabNavigator,
    Auth: AuthStack,
});

const styles = StyleSheet.create({
    icon: {
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 24,
    }
});

export default createAppContainer(AppNavigator)
