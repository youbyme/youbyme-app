import React from 'react';

// Navigators
import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
// Screens
import Initialising from "./../Layout/Initializing";
import Login from "./../Layout/Login";
import MainTabNavigator from './Navigation';

// Création des maps avec leurs Routes
// Map principal
const AppStack = createStackNavigator({
    Home: {
        screen: MainTabNavigator,
        navigationOptions: {
            header: null
        }
    }
});
// Map d'authentification
const AuthStack = createStackNavigator({
    SignIn: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    }
});


// Ajout des maps à un switch navigator qui gèrent les différents scénarios
const AppNavigator = createSwitchNavigator(
    {
        AuthLoading: Initialising,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    });

// Ajout du switchContainer à un AppContainer, appelé dans le render au sein du provider.
// Le provider permet de partager un store global au sein de l'application
const AppContainer = createAppContainer(AppNavigator);

export default AppContainer