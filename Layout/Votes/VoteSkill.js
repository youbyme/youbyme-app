import React, { Component } from 'react';

// Components
import {StyleSheet, View, FlatList} from 'react-native';

// Redux
import { connect } from 'react-redux'
import { ListItem  } from "react-native-elements";
import UserAvatar from "react-native-user-avatar";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import {getNewToken, getUnachievedSkillsList} from "../../API/Api";
import AsyncStorage from "@react-native-community/async-storage";

class VoteSkill extends Component {
    constructor(props) {
        super(props);

        this.state = {
            UnachievedSkills: [],
        };

        this.utilId = this.props.currentUser.utilId;
    }

    async handleNewToken(callback) {
        console.log('handleNewToken');
        const refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Sauvegarde du token dans le state
                this.state.token = tokens.token;

                // Save du token dans le store global
                this._actionSaveTheToken();

                this.setState({token: tokens.token});

                callback()
            } else {
                console.log("Go to authentification");

                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.navigation.navigate('Auth');
            }
        });
    }

    async _getUnachievedSkillsList() {
        if(this.props.token && this.props.vote.voteUser) {
            await getUnachievedSkillsList(this.props.vote.voteUser.utilId, this.props.token).then(async (skills) => {
                if(skills.code) {
                    await this.handleNewToken(() => this._getUnachievedSkillsList());
                } else {
                    this.setState({UnachievedSkills: []});
                    let arrUnachievedSkills = [];
                    if(skills) {
                        skills.forEach(
                            c => {
                                if (c.competenceParent) {
                                    arrUnachievedSkills.push({compNom: c.compNom, compId: c.compId, badgNom: c.badge.badgNom, badgImageUrl: c.badge.badgImageUrl})
                                }
                            }
                        );
                    }
                    this.setState({
                        UnachievedSkills: [...this.state.UnachievedSkills, ...arrUnachievedSkills]
                    })
                }
            });
        }
    }

    async componentDidMount() {
        await this._getUnachievedSkillsList();
    }

    _selectSkill(skill) {
        this.setState({VoteSkill: skill},
            ()=>{
                this._actionSaveVoteSkill();
                this.props.navigation.navigate('Votes', {VoteSkillSelected: skill});
            }
        );
    };

    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item }) => (
        <ListItem
            title={item.compNom}
            subtitle={item.compNom}
            leftAvatar={<UserAvatar
                size="50"
                name={(item.badgNom || 'Anonyme')}
                src={item.badgImageUrl || null}
            />}
            onPress = { (it) => {this._selectSkill(item)}}
        />
    );

    _itemChoose(item) {
        this.props.handleSelectedVoteSkill(item);
    }

    _displaySkillsList() {
        if (this.state.UnachievedSkills.length > 0) {
            return(
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.UnachievedSkills}
                    renderItem={this.renderItem}
                />
            );
        }
    }

    render () {
        return (
            <View>
                {this._displaySkillsList()}
            </View>
        )
    }

    _actionSaveVoteSkill() {
        console.log('ACTION _actionSaveVoteSkill');
        const action = { type: "SAVE_VOTE_SKILL", value: {voteSkill: this.state.VoteSkill} };
        this.props.dispatch(action);
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.storageLogin.token,
        currentUser: state.storageLogin.currentUser,
        vote: state.storageApp.vote
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VoteSkill);