import React, {Component} from 'react';

// Components
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import AppHeader from "../../Components/AppHeader/AppHeader";
import VoteUser from "./VoteUser";
import {Card, Button, Overlay} from "react-native-elements";
import UserAvatar from "react-native-user-avatar";
import VoteOverlay from "./../../Components/VoteOverlay/VoteOverlay";

// Style
import theme from './../../Assets/Styles/Theme';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import LinearGradient from 'react-native-linear-gradient';

// Redux
import {connect} from 'react-redux'

// API
import {getCurrentUser, getNewToken, makeAVote} from "../../API/Api";
import AsyncStorage from "@react-native-community/async-storage";

class Votes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            vote: {
                voteUser: null,
                voteSkill: null
            },
            displayOverlay: false,
            status: 'error'
        };

        this.utilId = this.props.currentUser.utilId;
    }

    async handleNewToken(callback) {
        console.log('handleNewToken');
        const refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Sauvegarde du token dans le state
                this.state.token = tokens.token;

                // Save du token dans le store global
                this._actionSaveTheToken();

                this.setState({token: tokens.token});

                callback()
            } else {
                console.log("Go to authentification");

                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.navigation.navigate('Auth');
            }
        });
    }

    _selectVoteUser = () => {
        console.log('going to navigate on VoteUser');
        this.props.navigation.navigate('VoteUser');
    };

    _selectVoteSkill = () => {
        console.log('going to navigate on VoteSkill');
        this.props.navigation.navigate('VoteSkill');
    };

    _displayVoteButton(utilPoint) {
        if (this.props.vote.voteUser !== '' && this.props.vote.voteSkill !== '') {
            let btnVoteDisabled = (this.voteUserSelected === null || this.voteSkillSelected === null || typeof(utilPoint) !== 'number' || utilPoint === 0) ? true : false;
            return (
                <Card>
                    <View style={[styles.voteButtonArea]}>
                        <Button
                            onPress={
                                () => {
                                    this.voteUserSelected = null;
                                    this.voteSkillSelected = null;
                                    this.props.navigation.setParams({
                                        VoteStudentSelected: null,
                                        VoteSkillSelected: null
                                    });
                                    this.setState({vote: {voteUser: null, voteSkill: null}});
                                    this._actionRemoveVoteInfos();
                                }
                            }
                            icon={
                                <FontAwesomeIcon
                                    name="undo"
                                    size={18}
                                    color="#2289dc"
                                />
                            }
                            title="&nbsp; Effacer"
                            buttonStyle={[styles.btnCancelVote]}
                            type="outline"
                        />
                        <Button
                            onPress={
                                async () => {
                                    await this._makeAVote();
                                }
                            }
                            icon={
                                <FontAwesomeIcon
                                    name="check"
                                    size={18}
                                    color={(btnVoteDisabled ? '#999' : '#ffffff')}
                                />
                            }
                            title="&nbsp; Envoyer le vote"
                            disabled={btnVoteDisabled}
                            containerStyle={[styles.btnValidVote]}
                        />
                    </View>
                </Card>
            )
        }
    }

    async _makeAVote () {
        await makeAVote(this.utilId, this.props.vote.voteUser.utilId, this.props.vote.voteSkill.compId, this.props.token).then(async (responseVote) => {
            if(responseVote.code) {
                await this.handleNewToken(() => this._makeAVote());
            } else {
                getCurrentUser(this.props.token).then((theCurrUser) => {
                    this.setState({currentUser: theCurrUser}, () => {
                        // Save du currentUser dans le store global
                        this._actionSaveCurrentUser();

                        this.voteUserSelected = null;
                        this.voteSkillSelected = null;

                        this.props.navigation.setParams({
                            VoteStudentSelected: null,
                            VoteSkillSelected: null
                        });

                        this.setState({vote: {voteUser: null, voteSkill: null}});
                        this._actionRemoveVoteInfos();

                        if(!responseVote.code) {
                            console.log('responseVote.code valid');
                            this.setState({displayOverlay: true, status: 'valid'});
                        } else {
                            console.log('responseVote.code error');
                            this.setState({displayOverlay: true, status: 'error'});
                        }
                    });
                });
            }
        }).catch((error) => {
            this.setState({displayOverlay: true, status: 'error'});
        })
    }

    _updateVoteUser = (user) => {
        this.setState({vote: {voteUser: user}});
    };

    _updateVoteSkill = (skill) => {
        this.setState({vote: {voteUser: {...this.state.vote.voteUser}, voteSkill: skill}});
    };

    render() {
        this.voteUser = this.props.vote.voteUser;
        this.voteSkill = this.props.vote.voteSkill;
        const utilPoint = this.props.currentUser.utilPointDispo;

        this.voteUserSelected = this.props.navigation.getParam('VoteStudentSelected', null);
        this.voteSkillSelected = this.props.navigation.getParam('VoteSkillSelected', null);

        let btnVoteSkillDisabled = ((this.voteUserSelected === null || typeof(utilPoint) !== 'number' || utilPoint === 0) ? true : false);

        return (
            <View style={styles.container}>
                <AppHeader title={'Voter'} Navigation={this.props.navigation}/>
                <View style={[theme.mainView]}>
                    <LinearGradient colors={[theme.bgMainGradientStart, theme.bgMainGradientEnd]} style={{flexGrow: 1}}>
                        <Card>
                            <Text style={styles.remainingVote}>
                                {utilPoint || 0} vote{utilPoint > 1 ? 's' : null} restant{utilPoint > 1 ? 's' : null}
                            </Text>
                        </Card>
                        <View>
                            <Card>
                                <TouchableOpacity onPress={() => this._selectVoteUser()}>
                                    <View style={styles.voteCard}>
                                        <View>
                                            <UserAvatar
                                                size="70"
                                                name={this.voteUser ? this.voteUser.utilPseudo : 'Anonyme'}
                                                src={this.voteUser ? this.voteUser.utilUrlAvatar : null}
                                            />
                                        </View>
                                        <View>
                                            <Text style={styles.titleCard}>
                                                {this.voteUser ? this.voteUser.utilPseudo : 'Choisir un étudiant'}
                                            </Text>
                                        </View>
                                        <View style={styles.cardIcon}>
                                            <FontAwesomeIcon
                                                name="caret-right"
                                                size={30}
                                                color="#333333"
                                            />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </Card>
                            <Card containerStyle = {(btnVoteSkillDisabled ? {backgroundColor: '#e3e6e8'} : null)}>
                                <TouchableOpacity onPress={() => this._selectVoteSkill()} disabled={btnVoteSkillDisabled}>
                                    <View style={styles.voteCard}>
                                        <View>
                                            <UserAvatar
                                                size="70"
                                                name={this.voteSkill ? this.voteSkill.compNom : 'Anonyme'}
                                                src={this.voteSkill ? (this.voteSkill.badgImageUrl ? this.voteSkill.badgImageUrl : null) : null}
                                            />
                                        </View>
                                        <View>
                                            <Text style={styles.titleCard}>
                                                {this.voteSkill ? this.voteSkill.compNom : 'Choisir une compétence'}
                                            </Text>
                                        </View>
                                        <View style={styles.cardIcon}>
                                            <FontAwesomeIcon
                                                name="caret-right"
                                                size={30}
                                                color="#333333"
                                            />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </Card>
                        </View>
                        <View>
                            {this._displayVoteButton(utilPoint)}
                        </View>
                    </LinearGradient>
                </View>
                <VoteOverlay isVisible = {this.state.displayOverlay} status = {this.state.status} handleCloseOverlay = {this._closeOverlay} />
            </View>
        );
    }

    _closeOverlay = () => {
        this.setState({displayOverlay: false})
    };

    _actionSaveCurrentUser() {
        console.log('ACTION _actionSaveCurrentUser');
        const action = {type: "SAVE_USER_INFO", value: {currentUser: this.state.currentUser}};
        this.props.dispatch(action);
    }

    _actionRemoveVoteInfos() {
        console.log('ACTION _actionRemoveVoteInfos');
        const action = {type: "REMOVE_VOTE_INFOS"};
        this.props.dispatch(action);
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    voteButtonArea: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    btnValidVote: {
        flex: 1,
        marginHorizontal: 8,
        marginVertical: 4,
    },
    btnCancelVote: {
        flex: 1,
        marginHorizontal: 8,
        marginVertical: 4,
    },
    remainingVote: {
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    voteCard: {
        width: '100%',
        marginHorizontal: 0,
        paddingHorizontal: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    titleCard: {
        fontSize: 20,
        marginLeft: 8
    },
    cardIcon: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        alignSelf: 'stretch',
        flexGrow: 1
    }
});

const mapStateToProps = (state) => {
    return {
        currentUser: state.storageLogin.currentUser,
        token: state.storageLogin.token,
        skills: state.storageApp.skills,
        vote: state.storageApp.vote
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => {
            dispatch(action)
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Votes);