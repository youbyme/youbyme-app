import React, { Component } from 'react';

// Components
import {StyleSheet, View, FlatList} from 'react-native';
import AppHeader from "../../Components/AppHeader/AppHeader";

// Redux
import { connect } from 'react-redux'
import { ListItem  } from "react-native-elements";
import UserAvatar from "react-native-user-avatar";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import {getNewToken, getVotableUsersList} from "../../API/Api";
import AsyncStorage from "@react-native-community/async-storage";

class VoteUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Who: [],
        };

        this.utilId = this.props.currentUser.utilId;
    }

    async handleNewToken(callback) {
        console.log('handleNewToken');
        const refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Sauvegarde du token dans le state
                this.state.token = tokens.token;

                // Save du token dans le store global
                this._actionSaveTheToken();

                this.setState({token: tokens.token});

                callback()
            } else {
                console.log("Go to authentification");

                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.navigation.navigate('Auth');
            }
        });
    }

    async componentDidMount() {
        await this._getVotableUsersList();
    }

    async _getVotableUsersList() {
        if(this.props.token) {
            await getVotableUsersList(this.props.currentUser.utilId, this.props.token).then(async (votablesUsers) => {
                if(votablesUsers.code) {
                    await this.handleNewToken(() => this._getVotableUsersList());
                } else {
                    let arrVotableUsers = [];
                    if(votablesUsers) {
                        votablesUsers.forEach(
                            v => arrVotableUsers.push({utilId: v.utilId, utilPseudo: v.utilPseudo, utilUrlAvatar: v.utilUrlAvatar, orgaNom: v.organisation.orgaNom})
                        );
                    }

                    this.setState({
                        Who: [...this.state.Who, ...arrVotableUsers]
                    })
                }
            });
        }
    }

    _selectStudent(student) {
        this.setState({VoteUser: student},
            ()=>{
                this._actionSaveVoteUser();
                this.props.navigation.navigate('Votes', {VoteStudentSelected: student});
            }
        );
    };

    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item }) => (
        <ListItem
            title={item.utilPseudo}
            subtitle={item.orgaNom}
            leftAvatar={<UserAvatar
                size="50"
                name={(item.utilPseudo || 'Anonyme')}
                src={item.utilUrlAvatar || null}
            />}
            onPress = {(it) => this._selectStudent(item)}
        />
    );

    _itemChoose(item) {
        this.props.handleSelectedVoteUser(item);
    }

    _displayStudentsList() {
        if (this.state.Who.length > 0) {
            return(
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.Who}
                    renderItem={this.renderItem}
                />
            );
        }
    }

    render () {
        return (
            <View>
                {this._displayStudentsList()}
            </View>
        )
    }

    _actionSaveVoteUser() {
        console.log('ACTION _actionSaveVoteUser');
        const action = { type: "SAVE_VOTE_USER", value: {voteUser: this.state.VoteUser} };
        this.props.dispatch(action);
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.storageLogin.token,
        currentUser: state.storageLogin.currentUser,
        vote: state.storageApp.vote
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VoteUser);
