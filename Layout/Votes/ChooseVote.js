import React, { Component } from 'react';
import {
    Text,
    View,
    Picker, Button
} from 'react-native';

import { getCurrentUser, getVotableUsersList, getUnachievedSkillsList, makeAVote } from './../../API/Api';

import { connect } from 'react-redux'

class ChooseVote extends Component {
  constructor(props) {
    super(props);

    this.state = {
        PickerUserValue: '',
        PickerSkillValue: '',
        Who: [],
        UnachievedSkills: []
    };

    this.utilId = this.props.currentUser.utilId;
  }

  _skillsUnachievedList(itemValue) {
      getUnachievedSkillsList(itemValue, this.props.token).then((skills) => {
          console.log("ChoseVote.js | _skillsUnachievedList | La liste des skillsUnachieved ", skills);
          this.setState({UnachievedSkills: []});
          let arrUnachievedSkills = [];
          if(skills) {
              skills.forEach(
                  c => {
                      if (c.competenceParent) {
                          arrUnachievedSkills.push({compNom: c.compNom, compId: c.compId})
                      }
                  }
              );
          }
          this.setState({
              UnachievedSkills: [...this.state.UnachievedSkills, ...arrUnachievedSkills]
          })
      });
  }

  _displayUnachievedSkillsList() {
      if(this.state.PickerUserValue !== null && this.state.PickerUserValue !== '') {
          return (
              <Picker
                  style={{width: '80%'}}
                  selectedValue = {this.state.PickerSkillValue}
                  onValueChange = {
                      (itemValue, itemIndex) => {
                          this.setState({
                              PickerSkillValue: itemValue
                          })
                      }
                  }
              >
                  <Picker.Item label={"Pour quelle compétence ?"} value={''}/>
                  {
                      this.state.UnachievedSkills.map( (comp) => {
                          return <Picker.Item key={comp.compId}  label={comp.compNom} value={comp.compId} />
                      })
                  }
              </Picker>
          )
      }
  }

  _displayVoteButton() {
      if(this.state.PickerUserValue !== '' && this.state.PickerSkillValue !== '') {
          return (
              <Button
                  onPress={
                      () => {
                          makeAVote(this.props.token, this.utilId, this.state.PickerUserValue, this.state.PickerSkillValue).then(() => {
                              getCurrentUser(this.props.token).then((theCurrUser) => {
                                  this.setState({currentUser: theCurrUser}, () => {
                                      // Save du currentUser dans le store global
                                      console.log('state de choseVote après setstae make a vote', this.state);
                                      this._actionSaveCurrentUser();
                                      this.props.navigation.navigate('Votes');
                                  });
                              });
                          });
                      }
                  }
                  title="Valider vote"
              />
          )
      }
  }

  componentDidMount() {
      if(this.props.token) {
          getVotableUsersList(this.props.currentUser.utilId, this.props.token).then((votablesUsers) => {
              console.log("ChoseVote.js | componentDidMount | La liste des votablesUsers ", votablesUsers);
              let arrVotableUsers = [];
              if(votablesUsers) {
                  votablesUsers.forEach(
                      v => arrVotableUsers.push({utilPseudo: v.utilPseudo, utilId: v.utilId})
                  );
              }

              this.setState({
                  Who: [...this.state.Who, ...arrVotableUsers]
              })
          });
      }
  }

  componentDidUpdate() {
      console.log('le state de ChoseVote après update', this.state);
  }

  render() {
    return(
      <View>
        <Picker
        style={{width: '80%'}}
        selectedValue = {this.state.PickerUserValue}
        onValueChange = {
            (itemValue, itemIndex) => {
                this.setState({
                    PickerUserValue: itemValue
                });

                (itemValue ? this._skillsUnachievedList(itemValue) : console.log('itemValue is empty') );
            }
        }
        >
            <Picker.Item label={"Pour qui ?"} value={null}/>
            {
                this.state.Who.map( (util) => {
                    return <Picker.Item key={util.utilId}  label={util.utilPseudo} value={util.utilId} />
                })
            }
        </Picker>
         { this._displayUnachievedSkillsList()}

         { this._displayVoteButton() }
      </View>
    )
  }

    _actionSaveCurrentUser() {
        console.log('ACTION _actionSaveCurrentUser');
        const action = { type: "SAVE_USER_INFO", value: {currentUser: this.state.currentUser} };
        this.props.dispatch(action);
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.storageLogin.currentUser,
        token: state.storageLogin.token,
        skills: state.storageApp.skills,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseVote)