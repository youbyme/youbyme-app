/*
Composant de chargement servant à rediriger l'utilisateur vers l'authentification ou vers l'accueil
 en fonction de la présence d'un token valide ou non.
*/
import React, { Component } from 'react'
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// API
import {getCurrentUser, getNewToken} from "../API/Api";

// Redux
import { connect } from 'react-redux'

class Initialising extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        };
    }

    async componentDidMount() {
        // Vérifie la présence d'un token et d'un refresh token dans l'AsyncStorage
        this.userToken = await AsyncStorage.getItem('userToken');
        this.refreshToken = await AsyncStorage.getItem('refreshToken');

        if (this.userToken) {
            this.setState({isLoading : true});

            // On teste la validité du token en récupérant les informations sur l'utilisateur courant.
            await getCurrentUser(this.userToken).then((userInfos) => {
                this.setState({isLoading: false});

                // Si on récupère l'email (login) de l'utilisateur, alors le token est encore valide, sinon on affiche l'erreur
                if (userInfos.utilEmail) {
                    // Ajout du token dans le state
                    this.setState({token: this.userToken});
                    // Save du token dans le store global
                    this._actionSaveTheToken();

                    // Ajout des infos de l'utilisateur dans le state
                    this.setState({currentUser: userInfos});
                    // Save des infos de l'utiliasteur dans le store global
                    this._actionSaveCurrentUser();

                    // Redirection vers l'accueil
                    return (this.props.navigation.navigate('App'))
                } else {
                    // On vérifie la présence du refresh token
                    if (this.refreshToken) {
                        this.setState({isLoading : true});

                        // Regénère un token et un refresh token
                        this._getNewToken();
                    } else {
                        if (userInfos.code) {
                            // Gestion du message d'erreur
                            console.log('erreur ', userInfos.code, ' : ', userInfos.message);
                        } else {
                            console.log('erreur inconnue', userInfos);
                        }

                        // On supprime le token
                        AsyncStorage.removeItem('userToken');
                        this._actionRemoveTheToken();

                        // On redirige l'utilisateur vers l'authentification
                        return(this.props.navigation.navigate('Auth'));
                    }
                }
            }).catch((error) => {
                console.log(error);
            });
        } else {
            // On redirige l'utilisateur vers l'authentification
            return(this.props.navigation.navigate('Auth'));
        }
    }

    async _getNewToken() {
        // On récupère les token
        await getNewToken(this.refreshToken).then((newToken) => {
            if (newToken.token) {
                // Save des tokens dans l'AsyncStorage
                AsyncStorage.setItem('userToken', newToken.token);
                AsyncStorage.setItem('refreshToken', newToken.refresh_token);

                // Save des variables de classe
                this.userToken = newToken.token;
                this.refreshToken = newToken.refresh_token;

                // Ajout du token dans le state
                this.setState({token: newToken.token});

                // Save du token dans le store global
                this._actionSaveTheToken();

                // Récupérer les infos de l'utlisateur
                this._getCurrentUser();
            } else {
                if (newToken.code) {
                    console.log('erreur ', newToken.code, ' : ', newToken.message);
                } else {
                    console.log('erreur inconnue', newToken);
                }

                // On redirige l'utilisateur vers l'authentification
                return(this.props.navigation.navigate('Auth'));
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    async _getCurrentUser() {
        // On récupère les infos de l'utilisateur
        await getCurrentUser(this.userToken).then((userInfos) => {
            this.setState({isLoading: false});
            // Si on récupère l'email (login) de l'utilisateur, alors le token est encore valide, sinon on affiche l'erreur
            if (userInfos.utilEmail) {
                // Ajout des infos de l'utilisateur dans le state
                this.setState({currentUser: userInfos});
                // Save des infos de l'utiliasteur dans le store global
                this._actionSaveCurrentUser();

                // Redirection vers l'accueil
                return(this.props.navigation.navigate('App'));
            } else {
                // Gestion du message d'erreur
                if (userInfos.code) {
                    console.log('erreur ', userInfos.code, ' : ', userInfos.message);
                } else {
                    console.log('erreur inconnue', userInfos);
                }

                // On supprime le token
                AsyncStorage.removeItem('userToken');
                AsyncStorage.removeItem('refreshToken');
                this._actionRemoveTheToken();
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size='large' />
                <Text style={styles.welcome}>Bienvenue</Text>
            </View>
        )
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = { type: "SAVE_THE_TOKEN", value: {token: this.state.token, refreshToken: this.state.refreshToken} };
        this.props.dispatch(action);
    }

    _actionRemoveTheToken() {
        console.log('ACTION _actionRemoveTheToken');
        const action = { type: "REMOVE_THE_TOKEN" };
        this.props.dispatch(action);
    }

    _actionSaveCurrentUser() {
        console.log('ACTION _actionSaveCurrentUser');
        const action = { type: "SAVE_USER_INFO", value: {currentUser: this.state.currentUser} };
        this.props.dispatch(action);
    }
}

const mapStateToProps = (state) => {
    return state.storageLogin
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

const styles = StyleSheet.create({
    welcome: {
        fontSize: 28
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Initialising)
