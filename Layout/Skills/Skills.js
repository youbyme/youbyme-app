import React, { Component } from 'react';

// Components
import { View } from 'react-native';
import AppHeader from './../../Components/AppHeader/AppHeader';
import SkillMenu from '../../Components/SkillMenu/SkillMenu';
import SkillViewer from '../../Components/SkillViewer/SkillViewer';

// Redux
import { connect } from 'react-redux'

// Functions
import {getNewToken, getSkillsList, getUserStats} from "../../API/Api";

// Style
import theme from './../../Assets/Styles/Theme';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from "@react-native-community/async-storage";

class Skills extends Component {
    constructor(props) {
        super(props);

        this.state = {
            parentSkills: [],
            childSkills: [],
            selectCategorySkill: null,
            selectedChildSkills: [],
            skillPercentage: []
        };

        this.colors = [
            {id: 1, background: '#8696cf', foreground: '#3454d1'},
            {id: 2, background: '#ffb0ea', foreground: '#ff47cb'},
            {id: 3, background: '#a166ff', foreground: '#6500ff'},
            {id: 4, background: '#c2ff8a', foreground: '#8aff24'},
            {id: 5, background: '#66d1ff', foreground: '#00b2ff'},
        ];

        this.userName = this.props.currentUser.utilPrenom + ' ' + this.props.currentUser.utilNom;

    }

    async handleNewToken(callback) {
        console.log('handleNewToken');
        let refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Sauvegarde du token dans le state
                this.state.token = tokens.token;

                // Save du token dans le store global
                this._actionSaveTheToken();

                this.setState({token: tokens.token});

                callback()
            } else {
                console.log("Go to authentification");

                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.navigation.navigate('Auth');
            }
        });
    }

    async _loadGetUserStats() {
        await getUserStats(this.props.currentUser.utilId, this.props.token).then(async (dataStats) => {
            if (dataStats.code) {
                await this.handleNewToken(() => this._loadGetUserStats());
            } else if(dataStats.length > 0) {
                let arrCompPercentage = [];

                dataStats.forEach((stats) => {
                    arrCompPercentage.push({
                        compId: stats.compId,
                        compNom: stats.competence,
                        compPercentage: parseInt(stats.percentage),
                    })
                });

                this.setState({skillPercentage: arrCompPercentage});
            }
        }).then(() => {
            // Méthode pour récupérer les compétences de l'API
            this._loadSkills();
        }).catch(error => {
            console.log(error);
        });
    }

    async componentDidMount() {
        await this._loadGetUserStats();
    }

    async _loadSkills() {
        await getSkillsList(this.props.token).then(async (skills) => {
            if (skills.code) {
                await this.handleNewToken(() => this._loadSkills());
            } else {
                // Tableaux pour distinguer catégories mère et enfants
                let arrChildSkills = [];
                let arrParentSkills = [];

                // Pour chacune des catégories de l'API, si competenceParent est null alors c'est une compétence parent.
                if(skills && this.state.skillPercentage.length > 0) {
                    skills.forEach(
                        c => {
                            if (c.competenceParent !== null) {
                                let skillPercentage = this.state.skillPercentage.find(s => s.compId === c.compId);

                                arrChildSkills.push({
                                    compId: c.compId,
                                    compNom: c.compNom,
                                    compDescription: c.compDescription,
                                    competenceParent: c.competenceParent.compId,
                                    compPercentage: (skillPercentage ? skillPercentage.compPercentage : 0)
                                });
                            } else {
                                arrParentSkills.push({
                                    compId: c.compId,
                                    compNom: c.compNom,
                                    compDescription: c.compDescription
                                });
                            }
                        }
                    );

                    // On met à jour le state avec les différentes compétences
                    this.setState({
                        parentSkills: [...arrParentSkills],
                        childSkills: [...arrChildSkills]
                    });

                    // On les save dans le store global
                    this._actionSaveSkillsList();
                }
            }
        });
    }

    _updateChildSkills = (comp) => {
        // On met à jour le state avec la compétence selectionnée
        comp.compForegroundColor = this.colors[(comp.compId % (this.colors.length - 1) )].foreground;
        comp.compBackgroundColor = this.colors[(comp.compId % (this.colors.length - 1) )].background;
        this.setState({selectCategorySkill: comp});

        // Tableaux pour stocker les compétences enfants selon leur parent
        let arrSelectedChildSkills = [];

        // On parcours toutes les compétences enfants pour ne sélectionner que les bonnes
        this.state.childSkills.map((c) => {
            if(c.competenceParent === comp.compId) {
                arrSelectedChildSkills.push(c);
            }
        });

        // On met à jour le state des compétences enfants sélectionnées
        this.setState({selectedChildSkills: arrSelectedChildSkills})
    };

    _loadSkillsComponents() {
        // Si on a des compétences parents alors on render le menu et le swiper
        // SkillMenu:
            // parentSkills = liste des compétences parents
            // handleSelectedCategory = handler pour récupérer la compétence parent sélectionnée dans le menu
        // SkillViewer
            // childSkills = liste des compétences enfants
            // selectCategorySkill = Catégorie sélectionnée avec couleurs
        if(this.state.parentSkills !== null && this.state.parentSkills !== '') {
            if(this.state.parentSkills.length > 0) {
                return (
                    <View style={{flex: 1}}>
                        <View style={{flex: 1}}>
                            <SkillMenu parentSkills={this.state.parentSkills} handleSelectedCategory={this._updateChildSkills}/>
                        </View>
                        <View style={{flex: 8}}>
                            <SkillViewer childSkills={this.state.selectedChildSkills} selectCategorySkill={this.state.selectCategorySkill}/>
                        </View>
                    </View>
                )
            }
        }
    }


    render(){
        return (
            <View style={{flex: 1}}>
                <AppHeader title={'Mes compétences'} Navigation={this.props.navigation}/>
                <View style={[theme.mainView]}>
                    <LinearGradient colors={[theme.bgMainGradientStart, theme.bgMainGradientEnd]} style={{flexGrow: 1}}>
                        { this._loadSkillsComponents() }
                    </LinearGradient>
                </View>
            </View>
        );
    }

    // Save dans le store global les listes des compétences selon leur type parent/enfant
    _actionSaveSkillsList() {
        console.log('ACTION _actionSaveSkillsList');
        const action = { type: "SAVE_SKILL_LIST", value: {parentSkills: this.state.parentSkills, childSkills: this.state.childSkills} };
        this.props.dispatch(action);
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.storageLogin.token,
        currentUser: state.storageLogin.currentUser,
        parentSkills: state.storageApp.parentSkills,
        childSkills: state.storageApp.childSkills,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Skills)
