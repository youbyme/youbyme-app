import React, {Component} from 'react';

// Components
import {StyleSheet, Text, View} from 'react-native';
import AppHeader from './../../Components/AppHeader/AppHeader';
import BadgeMenu from '../../Components/BadgeMenu/BadgeMenu';
import BadgeViewer from '../../Components/BadgeMenu/BadgeViewer';
// Redux
import {connect} from 'react-redux';
// Functions
import {getBadgesList, getCurrentUser, getNewToken, getAssignations} from '../../API/Api';

// Style
import theme from './../../Assets/Styles/Theme';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from "@react-native-community/async-storage";

class Badges extends Component {
    constructor(props) {
        super(props);

        this.state = {
            parentBadges: [],
            childBadges: [],
            childBadgesToDisplay: [],
            assignations: []
        };
    }

    async handleNewToken(callback) {
        const refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Save du token dans le store global
                this._actionSaveTheToken();
                callback()
            } else {
                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.navigation.navigate('Auth');
            }
        });
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <AppHeader title={'Mes badges'} Navigation={this.props.navigation}/>
                <View style={[theme.mainView]}>
                    <LinearGradient colors={[theme.bgMainGradientStart, theme.bgMainGradientEnd]} style={{flexGrow: 1}}>
                        <View style={{flex: 1}}>
                            <BadgeMenu parentBadges={this.state.parentBadges}
                                       handleSelectedCategory={this._updateChildBadges}/>
                        </View>
                        <View style={{flex: 8}}>
                            <BadgeViewer childBadges={this.state.childBadgesToDisplay}
                                         assignations={this.state.assignations}/>
                        </View>
                    </LinearGradient>
                </View>
            </View>
        )
    }

    componentWillMount() {
        this._loadBadges();
        this._loadBadgesUser();
    }

    async _loadBadgesUser() {
      /*  let idBadgesRecus = [];
        await getCurrentUser(this.props.token).then(async (user) => {
            if(user.code) {
                await this.handleNewToken(() => this._loadBadgesUser());
            } else {
                for (badge of user.badges) {
                    idBadgesRecus.push(badge.badgId);
                }
                this.setState({receivedBadgesId: idBadgesRecus})
            }
        });*/
        await getAssignations(this.props.currentUser.utilId, this.props.token).then(async (assignations)=> {
          if(assignations.code) {
              await this.handleNewToken(() => this._loadBadgesUser());
          } else {
            this.setState({assignations: assignations});
          }
        });
    }

    async _loadBadges() {
        await getBadgesList(this.props.token).then(async (badges) => {
            if(badges.code) {
                await this.handleNewToken(() => this._loadBadges());
            } else {
                let arrChildBadges = [];
                let arrParentBadges = [];

                //tri des parents & enfants
                badges.forEach(
                    badge => {
                        if (badge.competence.competenceParent !== null) {
                            arrChildBadges.push(badge);
                        } else {
                            arrParentBadges.push(badge);
                        }
                    });
                this.setState({
                    childBadges: [...arrChildBadges],
                    parentBadges: [...arrParentBadges]
                });
            }
        });
    }

    _updateChildBadges = (badgeParent) => {
        let arrChildBadgesToDisplay = [];

        this.state.childBadges.map((badge) => {
            if (badge.competence.competenceParent.compId === badgeParent.competence.compId) {
                arrChildBadgesToDisplay.push(badge);
            }
        });
        this.setState({childBadgesToDisplay: arrChildBadgesToDisplay});
    };

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const size = 200;
const width = 15;
const cropDegree = 90;
const textOffset = width;
const textWidth = size - (textOffset * 2);
const textHeight = size * (1 - cropDegree / 360) - (textOffset * 2);

const styles = StyleSheet.create({
    textView: {
        position: 'absolute',
        top: textOffset,
        left: textOffset,
        width: textWidth,
        height: textHeight,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 20,
    },
});

const mapStateToProps = (state) => {
    return {
        currentUser: state.storageLogin.currentUser,
        token: state.storageLogin.token
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => {
            dispatch(action)
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Badges)
