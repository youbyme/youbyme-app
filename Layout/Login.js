import React from 'react';
import {View, TextInput, StyleSheet, Alert, ActivityIndicator, Text} from 'react-native';
import {Button} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';

// Redux
import {connect} from 'react-redux';

// Connexion à l'API
import {getCurrentUser, loginAPI} from "../API/Api";
import {Card, Image} from "react-native-elements";

// Style
import theme from './../Assets/Styles/Theme';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import LinearGradient from 'react-native-linear-gradient';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.login = "";
        this.password = "";
        this.state = {
            isLoading: false,
            signInError: false
        };

        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    // Permet l'authentification via JWT token à l'API et redirige vers la bonne route
    async _signInAsync() {
        this.setState({isLoading: true});

        // Connexion à l'API
        await loginAPI(this.login, this.password).then((token) => {
            // Si l'API renvoie un token on l'enregistre dans l'AsyncStorage puis redirection
            if (token.token) {
                console.log('Login OK');

                // Ajout du token dans le state
                this.setState({token: token.token});

                // Save du token dans le store global
                this._actionSaveTheToken();

                // On save le token dans l'AsyncStorage
                AsyncStorage.setItem('userToken', token.token);
                AsyncStorage.setItem('refreshToken', token.refresh_token);

                getCurrentUser(token.token).then((userInfos) => {
                    // Ajout du token dans le state
                    this.setState({currentUser: userInfos});

                    // Save du token dans le store global
                    this._actionSaveCurrentUser();

                    // Chargement terminé
                    this.setState({isLoading: false});

                    console.log('Authentification OK');

                    // Redirige vers l'acceil
                    this.props.navigation.navigate('App');
                });

            } else if (token.code) {
                console.log('erreur ', token.code, ' : ', token.message);
                this.setState({isLoading: false, signInError: true});
            } else {
                console.log('erreur inconnue', token);
                this.setState({isLoading: false, signInError: true});
            }

            if (this.state.signInError) {
                Alert.alert(
                    'Connexion impossible',
                    (token.message || 'Erreur inconnue'),
                    [{text: 'OK', onPress: () => console.log('OK Pressed')}],
                );
                this.setState({signInError: false});
            }
        });
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }

    _actionSaveCurrentUser() {
        console.log('ACTION _actionSaveCurrentUser');
        const action = {type: "SAVE_USER_INFO", value: {currentUser: this.state.currentUser}};
        this.props.dispatch(action);
    }

    render() {
        return (
            <View style={[styles.main_container, theme.bgPrimary]}>
                <View>
                    <Image
                        source={require('./../Assets/Images/logo_youbyme_nico_v1-white.png')}
                        resizeMode="contain"
                        PlaceholderContent={<ActivityIndicator />}
                        style = {{width: '92%', height: 200, marginBottom: 0, alignSelf: 'center'}}
                    />
                </View>
                <View style={styles.login_container}>
                    <Card>
                        <TextInput style={styles.textinput}
                                   placeholder='Login'
                                   autoCapitalize="none"
                                   autoCorrect={false}
                                   keyboardType = {'email-address'}
                                   blurOnSubmit={ false }
                                   onSubmitEditing={() => {
                                       this.focusNextField('password');
                                   }}
                                   ref={ input => {
                                       this.inputs['login'] = input;
                                   }}
                                   returnKeyType={ "next" }
                                   onChangeText={(login) => this._loginTextChanged(login)}/>
                        <TextInput style={styles.textinput}
                                   placeholder='Password'
                                   autoCapitalize="none"
                                   autoCorrect={false}
                                   secureTextEntry={true}
                                   blurOnSubmit={ true }
                                   returnKeyType={ "done" }
                                   ref={ input => {
                                       this.inputs['password'] = input;
                                   }}
                                   onSubmitEditing = {() => this._signInAsync()}
                                   onChangeText={(password) => this._passwordTextChanged(password)}/>
                        <Button
                            title='&nbsp; Connexion'
                            icon={
                                <FontAwesomeIcon
                                    name="sign-in-alt"
                                    size={15}
                                    color="white"
                                />
                            }
                            onPress={() => this._signInAsync()}
                            style={styles.validButton}
                            loading = {this.state.isLoading}
                            ViewComponent={LinearGradient} // Don't forget this!
                            linearGradientProps={{
                                colors: [theme.btnGradientStart, theme.btnGradientEnd],
                                start: { x: 0.5, y: 0 },
                                end: { x: 0.5, y: 1 },
                            }}
                        />
                    </Card>
                </View>
            </View>
        )
    }

    // Met à jour les variables de classe
    _loginTextChanged(login) {
        this.login = login;
    }

    _passwordTextChanged(password) {
        this.password = password;
    }

}

// Redux State and Dispatch Update
const mapStateToProps = (state) => {
    return state.storageLogin
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => {
            dispatch(action)
        }
    }
};

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        justifyContent: 'center'
    },
    login_container: {
        height: '44%',
        width: '92%',
        marginLeft: '4%',
        paddingHorizontal: '4%',
        paddingVertical: '4%',
        justifyContent: 'space-between',
    },
    textinput: {
        borderColor: '#fff',
        borderWidth: 1,
        paddingLeft: 5
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    validButton: {}
});

// Connection au state et props de l'application
export default connect(mapStateToProps, mapDispatchToProps)(Login)
