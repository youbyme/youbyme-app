const theme = {
    bgPrimary: {
        backgroundColor: '#111e2b'
    },
    bgSecondary: {
        backgroundColor: '#ffc21d'
    },
    bgHeader: {
        backgroundColor: '#2289dc'
    },
    btnGradientStart: '#3e99e2',
    btnGradientEnd: '#3678ad',
    bgMainGradientStart: '#F6EBDE',
    bgMainGradientEnd: '#EBB8CC',
    mainView: {
        flexGrow: 1
    },
    bgCatItemDefault: '#444444',
    bgCatItemSelected: '#cbcbcb',
    textCatItemDefault: '#fff',
    textCatItemSelected: '#222',
    bgTabBarActive: '#dddddd',
    bgTabBarInactive: '#ffffff',
};

export default theme