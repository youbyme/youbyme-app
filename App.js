import React from 'react';

// Redux
import { Provider } from 'react-redux'
import Store from './Store/Store'

import AppContainer from './Navigation/LoginNavigation';

export default class App extends React.Component {
  render() {
    return (
        <Provider store={Store}>
            <AppContainer/>
        </Provider>
    )
  }
}
