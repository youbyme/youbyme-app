const initialAppState = {
    isLoading: true,
    error: null,
    parentSkills: [],
    childSkills: [],
    badges: [],
    vote: {
        voteUser: null,
        voteSkill: null
    }
};

/* Liste les différentes actions et gère le state
Copie l'ancien state et les infos dans un nouveau state global à l'application
Car state est immuable */
const appReducer = (state = initialAppState, action) => {
    switch (action.type) {
        case 'LOADING':
            return { ...state, loading: action.isLoading };
        case 'ERROR':
            return { ...state, error: action.error };
        case 'SAVE_SKILL_LIST':
            console.log('SAVE_SKILL_LIST', action.value);
            return {...state, parentSkills: action.value.parentSkills, childSkills: action.value.childSkills};
        case 'SAVE_BADGES_LIST':
            return {...state, badges: action.value.badges};
        case 'SAVE_VOTE_USER':
            return {...state, vote: {voteUser: action.value.voteUser}};
        case 'SAVE_VOTE_SKILL':
            return {...state, vote: { voteUser: {...state.vote.voteUser}, voteSkill: action.value.voteSkill }};
        case 'REMOVE_VOTE_INFOS':
            return {...state, vote: { voteUser: null, voteSkill: null }};
        case 'LOG_OUT':
            return {...state, parentSkills: [], childSkills: [], badges: [], vote: { voteUser: null, voteSkill: null }};
        default:
            return state;
    }
};

export default appReducer;