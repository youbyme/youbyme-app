const initialLoginState = {
    token: {},
    isLoading: true,
    error: null,
    currentUser: [],
};

/* Liste les différentes actions et gère le state
Copie l'ancien state et les infos dans un nouveau state global à l'application
Car state est immuable */
const loginReducer = (state = initialLoginState, action) => {
    switch (action.type) {
        case 'LOADING':
            return { ...state, loading: action.isLoading };
        case 'ERROR':
            return { ...state, error: action.error };
        case 'SAVE_THE_TOKEN':
            return { ...state, token: action.value.token };
        case 'REMOVE_THE_TOKEN':
            return { ...state, token: null };
        case 'SAVE_USER_INFO':
            return { ...state, currentUser: action.value.currentUser };
        default:
            return state;
    }
};

export default loginReducer;
