import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import ReduxThunk from 'redux-thunk';
import loginReducer from './Reducers/loginReducer';
import appReducer from './Reducers/appReducer';

const middlewares = [ReduxThunk];

const rootReducer = combineReducers({
    storageLogin: loginReducer,
    storageApp: appReducer
});

// Créer le store global à partir du reducer login
const Store = createStore(
    rootReducer,
    {},// default state of the application
    compose(applyMiddleware(...middlewares)),
);


export default Store;