import React, { Component } from 'react';
import {View, Text, StyleSheet, Modal, TouchableHighlight, Dimensions, TouchableOpacity} from 'react-native';

// Redux
import { connect } from 'react-redux'
import AsyncStorage from "@react-native-community/async-storage";

// API
import {getNewToken, getUserStats} from "../API/Api";

// Components
import { Card, Button } from 'react-native-elements'
import UserAvatar from "react-native-user-avatar";
import { PieChart } from 'react-native-chart-kit';

// Style
import theme from './../Assets/Styles/Theme';
import randomColor from 'randomcolor';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import LinearGradient from 'react-native-linear-gradient';

class ProfileModal extends Component {

    constructor(props) {
        super(props);
        this.userName = this.props.currentUser.utilPrenom + ' ' + this.props.currentUser.utilNom;
        this.screenWidth = Dimensions.get('window').width;
        this.state = {
            graphData: []
        };
    }


    async handleNewToken(callback) {
        console.log('handleNewToken');
        const refreshToken = await AsyncStorage.getItem('refreshToken');

        await getNewToken(refreshToken).then(async (tokens) => {
            if(tokens.token) {
                // On save le token dans l'AsyncStorage
                await AsyncStorage.setItem('userToken', tokens.token);
                await AsyncStorage.setItem('refreshToken', tokens.refresh_token);

                // Sauvegarde du token dans le state
                this.state.token = tokens.token;

                // Save du token dans le store global
                this._actionSaveTheToken();

                this.setState({token: tokens.token});

                callback()
            } else {
                console.log("Go to authentification");

                await AsyncStorage.removeItem('userToken');
                await AsyncStorage.removeItem('refreshToken');

                this.props.handleSignOut();
            }
        });
    }

    async componentDidMount() {
        await this._loadGetUserStats();
    }

    async _loadGetUserStats() {
        await getUserStats(this.props.currentUser.utilId, this.props.token).then(async (dataStats) => {
            if (dataStats.code) {
                await this.handleNewToken(() => this._loadGetUserStats());
            } else {
                if (dataStats) {
                    let arrFormatData = [];
                    let totalComp = 0;

                    dataStats.forEach((stats) => {
                        totalComp += stats.percentage
                    });

                    dataStats.forEach((stats) => {
                        if (stats.percentage > 0) {
                            arrFormatData.push({
                                name: '% ' + stats.competence,
                                competence: parseInt((100 / totalComp) * stats.percentage),
                                color: randomColor({luminosity: 'bright'}),
                                legendFontColor: '#7F7F7F',
                                legendFontSize: 15
                            })
                        }
                    });

                    this.setState({graphData: arrFormatData});
                }
            }
        });
    }

    _userStatGraph() {
        const chartConfig = {
            backgroundGradientFrom: '#1E2923',
            backgroundGradientTo: '#08130D',
            color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            strokeWidth: 2 // optional, default 3
        };

        return (
            <PieChart
                data={this.state.graphData}
                width={this.screenWidth}
                height={200}
                chartConfig={chartConfig}
                accessor="competence"
                backgroundColor="transparent"
                paddingLeft="0"
                absolute
                style = {styles.pieStats}
            />
        )
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.handleClose()
                }}
            >
                <View style={[styles.modalContent, theme.bgPrimary]}>
                    <View style={{flex: 1, padding: 0}}>
                        <View style={{marginHorizontal: 0, paddingHorizontal: 0, width: "100%"}}>
                            <TouchableHighlight
                                onPress={() => {
                                    this.props.handleClose()
                                }}
                                style = {
                                    styles.closeIcon
                                }
                            >
                                <FontAwesomeIcon
                                    name="angle-down"
                                    style = {{ color: '#ffffff', fontSize: 24, marginRight: 16 }}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={{flex: 5}}>
                            <Card containerStyle={styles.profileCard}>
                                <View style = { styles.cardAvatar }>
                                    <UserAvatar
                                        size = "150"
                                        name = {(this.props.currentUser.utilPseudo) ? this.props.currentUser.utilPseudo : (this.userName || 'Anonyme')}
                                        src = {this.props.currentUser.utilUrlAvatar || null}
                                    />
                                </View>
                                <View style={styles.profileLegend}>
                                    <Text style={styles.legendName}>{ this.props.currentUser.userName || this.userName }</Text>
                                    <Text style={styles.legendSubtile}>{ this.props.currentUser.organisation.orgaNom || null }</Text>
                                </View>
                            </Card>
                        </View>
                        <View style={{ flex: 5 }}>
                            <Card containerStyle={styles.statsCard}>
                                <Text style={{fontWeight: 'bold', fontSize: 24, alignSelf: 'center'}} > Mon profil </Text>
                                { this._userStatGraph() }
                            </Card>
                        </View>
                        <View style={styles.buttonArea}>
                            <Button
                                onPress={() => {
                                    this.props.handleSignOut()
                                }}
                                title="&nbsp; Se déconnecter"
                                icon={
                                    <FontAwesomeIcon
                                        name="sign-out-alt"
                                        size={15}
                                        color="white"
                                    />
                                }
                                ViewComponent={LinearGradient} // Don't forget this!
                                linearGradientProps={{
                                    colors: [theme.btnGradientStart, theme.btnGradientEnd],
                                    start: { x: 0.5, y: 0 },
                                    end: { x: 0.5, y: 1 },
                                }}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }

    _actionSaveTheToken() {
        console.log('ACTION _actionSaveTheToken');
        const action = {type: "SAVE_THE_TOKEN", value: {token: this.state.token}};
        this.props.dispatch(action)
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.storageLogin.currentUser,
        token: state.storageLogin.token
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
    },
    buttonArea: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 10,
    },
    profileCard: {
        justifyContent: 'center',
        marginVertical: 0,
        marginHorizontal: 10,
    },
    statsCard: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 0,
        marginHorizontal: 10,
    },
    pieStats: {
        marginVertical: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardAvatar: {
        alignItems: 'center',
    },
    profileLegend: {
        width: '100%',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    legendName: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    legendSubtile: {
        fontSize: 22,
    },
    closeIcon: {
        alignItems: 'flex-end',
        marginTop: 8
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileModal)