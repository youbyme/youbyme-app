/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// Définit App.js comme premier composant de l'application
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => App);
