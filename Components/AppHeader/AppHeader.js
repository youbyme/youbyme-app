import React, { Component } from 'react';

// Components
import { Header } from 'react-native-elements';
import UserAvatar from "react-native-user-avatar";

// Redux
import { connect } from 'react-redux'
import {Platform, StyleSheet, TouchableOpacity, View} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import ProfileModal from "../../Modals/ProfileModal";

// Style
import theme from './../../Assets/Styles/Theme';

class AppHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            opened: false
        }
    }

    openProfileModal = () => {
        this.setState({
            opened: true
        });
    };

    closeProfileModal = () => {
        this.setState({
            opened: false
        });
    };

    _signOutAsync = () => {
        this.setState({
            opened: false
        });
        AsyncStorage.removeItem('userToken');
        AsyncStorage.removeItem('refreshToken');
        this._actionLogout();
        this.props.Navigation.navigate('Auth');
        console.log('Logout ok');
    };

    render() {
        return (
            <View>
                <Header
                    containerStyle={[styles.headerMenu,theme.bgHeader]}
                    leftComponent={
                        <TouchableOpacity
                            onPress={this.openProfileModal}
                        >
                            <UserAvatar
                                size="40"
                                name={(this.props.utilPseudo) ? this.props.utilPseudo : (this.userName || 'Anonyme')}
                                src={this.props.currentUser.utilUrlAvatar || null}
                            />
                        </TouchableOpacity>
                    }
                    centerComponent={{text: this.props.title, style: {color: '#fff', fontSize: 20, fontWeight: 'bold'}}}
                />
                <ProfileModal handleClose={this.closeProfileModal} handleSignOut={this._signOutAsync} currentUser={this.state.currentUser} visible={this.state.opened} />
            </View>
        )
    }

    _actionLogout() {
        console.log('ACTION _actionLogout');
        const action = {type: "LOG_OUT"};
        this.props.dispatch(action);
    }
}

const styles = StyleSheet.create({
    headerMenu: {
        height: Platform.OS === 'ios' ? 70 :  70 - 16,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 22,
        paddingBottom: 22
    }
});

const mapStateToProps = (state) => {
    return {
        token: state.storageLogin.token,
        currentUser: state.storageLogin.currentUser,
        parentSkills: state.storageApp.parentSkills,
        childSkills: state.storageApp.childSkills,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader)