import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {Card, Button, Overlay} from "react-native-elements";
import theme from './../../Assets/Styles/Theme';

class VoteOverlay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            status: this.props.status,
        };

        this.overlayContent = {
            valid: {
                message: 'Votre vote a bien été envoyé.',
                image: require('./../../Assets/Images/valid.png')
            },
            error: {
                message: 'Erreur avec votre vote.',
                image: require('./../../Assets/Images/error.png')
            }
        };
    }

    render() {
        return (
                <Overlay isVisible={this.props.isVisible}
                         overlayStyle = {styles.overlayStyle}
                         height = {'44%'} width = {'66%'}
                         onBackdropPress={() => this.props.handleCloseOverlay()}>
                    <TouchableOpacity onPress = {() => this.props.handleCloseOverlay()}>
                        <View style={{width: '80%', alignItems: 'center'}}>
                            <Image
                                source = {this.props.status === 'valid' ? this.overlayContent.valid.image : this.overlayContent.error.image}
                                style = {styles.overlayImage}
                            />
                            <Text style={styles.overlayMessage}>
                                {this.props.status === 'valid' ? this.overlayContent.valid.message : this.overlayContent.error.message}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </Overlay>
        )
    }
}

const styles = StyleSheet.create({
    overlayStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    overlayImage: {
        marginBottom: 20
    },
    overlayMessage: {
        fontSize: 18
    }
});

export default VoteOverlay