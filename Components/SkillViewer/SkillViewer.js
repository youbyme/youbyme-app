import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    Text,
    Dimensions
} from 'react-native';

import Swiper from 'react-native-swiper';
import AnimatedGaugeProgress from "react-native-simple-gauge/src/AnimatedGaugeProgress";

class SkillViewer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            childSkills: this.props.childSkills,
            selectCategorySkill: (this.props.selectCategorySkill || {compBackgroundColor: '#b0c4de', compForegroundColor: '#333333'})
        };
    }

    // Charge le composant Swiper avec les bonnes compétences enfants s'il y en a
    _loadSwiperComponent() {
        if(this.props.childSkills !== null) {
            return (
                <Swiper style={styles.wrapper} showsButtons={true}>
                    {
                        // On map les compétences enfants pour afficher les slides
                        this.props.childSkills.map((comp) => {
                            return (
                                <View style={[styles.slide]} key={comp.compId}>
                                    <AnimatedGaugeProgress
                                        size={size}
                                        width={width}
                                        cropDegree={cropDegree}
                                        fill={parseInt(comp.compPercentage) || 0}
                                        rotation={90}
                                        tintColor={this.props.selectCategorySkill.compForegroundColor}
                                        delay={0}
                                        stroke={[4, 4]} //For a equaly dashed line
                                        strokeCap="circle"
                                        style = {styles.animatedGauge}
                                    >
                                        <View style={styles.textGaugeContainer}>
                                            <Text style={styles.textGauge}>{comp.compPercentage || 0}%</Text>
                                        </View>
                                    </AnimatedGaugeProgress>
                                    <View style={styles.textView}>
                                        <Text style={styles.compNom}>{comp.compNom}</Text>
                                    </View>
                                    <Text style={styles.compDescription}>{comp.compDescription}</Text>
                                </View>
                            );
                        })
                    }
                </Swiper>
            )
        }
    }

    render(){
        return (
            <View style={{flex: 4}}>
                { this._loadSwiperComponent() }
            </View>
        );
    }
}

const size = 270;
const width = 22;
const cropDegree = 90;
const textOffset = width;
const textWidth = size - (textOffset*2);
const textHeight = size*(1 - cropDegree/360) + (textOffset * 0.5);

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    compNom: {
        color: '#fff',
        fontSize: 28,
        fontWeight: 'bold',
    },
    compDescription: {
      fontSize: 20
    },
    animatedGauge: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    textGaugeContainer: {
        position: 'absolute',
        top: textOffset,
        left: textOffset,
        height: textHeight,
        width: textWidth,
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold'
    },
    textGauge: {
        fontSize: 40,
        alignItems: 'center',
        color: '#ffffff'
    },
    textView: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default SkillViewer