import React, { Component } from 'react';

// Components
import { StyleSheet, View } from 'react-native';
import Category from 'react-native-category';

// Style
import theme from './../../Assets/Styles/Theme';

class SkillMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            parentSkills: this.props.parentSkills,
            selectedCategory: null
        };
    }

    // Event du menu, remonte la compétence sélectionnée à l'handler du parent
    _itemChoose(item) {
        this.props.handleSelectedCategory(item);
    }

    // Charge le composant Category avec les compétences parentes si elles ne sont pas nulles
    _loadCategoryComponent() {
        if(this.props.parentSkills !== null) {
            return (
                <Category
                    data={this.props.parentSkills}
                    itemSelected={(item) => this._itemChoose(item)}
                    itemText={'compNom'}  //set attribule of object show in item category
                    bounces={true}
                    textType={'capitalize'}
                    colorTextDefault={theme.textCatItemDefault}
                    colorTextSelected={theme.textCatItemSelected}
                    colorItemDefault={theme.bgCatItemDefault}
                    colorItemSelected={theme.bgCatItemSelected}
                    style={theme.bgSecondary}
                />
            )
        }
    }

    render(){
        return (
            <View style={{flex: 1}}>
                { this._loadCategoryComponent() }
            </View>
        );
    }

}

export default SkillMenu