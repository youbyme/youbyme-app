import React, { Component } from 'react';

import {
    StyleSheet,
    View, Text, Image,
    TouchableOpacity
} from 'react-native';
import BadgeDescription from './BadgeDescription';
import { FlatGrid } from 'react-native-super-grid';

import Swiper from 'react-native-swiper';

class BadgeViewer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      badgeToDescribe: null
    }
  }

  _renderBadges() {
    if (this.props.childBadges !== null) {
       return (
        <FlatGrid itemDimension={130}
                  items={this.props.childBadges}
                  renderItem={( badge ) => (this._renderBadge(badge.item))}
        />
      )
    }
  }

  _displayBadgeOverlay(badge) {
    this.setState({
      badgeToDescribe: badge
    });
  }

  _renderBadge(badge) {
    let received = this._getDateObtention(badge) !== null;
    return (
        <TouchableOpacity
        onPress={() => this._displayBadgeOverlay(badge)}
        style={styles.badgeWrapper}>
        {
          received ? null : <View style={{ width: 150, height: 150,
          borderRadius: 100, position: 'absolute',
          opacity: 0.7, backgroundColor: '#333'}}
          />
        }
          <Image
          style={received ? styles.badgeReceived : styles.badgeNotReceived}
          source={{uri: badge.badgImageUrl}}
          />

        </TouchableOpacity>
    );

  }

  _hideOverlay() {
    this.setState({badgeToDescribe: null});
  }

  _renderOverlay() {
    if (this.state.badgeToDescribe !== null) {
      return (
        <BadgeDescription
        badge={this.state.badgeToDescribe}
        dateObtention={this._getDateObtention(this.state.badgeToDescribe)}
        onBackdropPress={() => this._hideOverlay()}
        />
      );
    }
  }

  render() {
    return (
        <View style={{flex: 1}}>
        { this._renderOverlay() }
        { this._renderBadges() }
        </View>
      );
    }

  _getDateObtention(badge) {
    for (assignation of this.props.assignations) {
      if (assignation.badge.badgId === badge.badgId) {
        return assignation.date;
      }
    }
    return null;
  }


}

const styles = StyleSheet.create({
  badgeReceived: {
    width: 150,
    height: 150
  },
  badgeNotReceived: {
    width: 150,
    height: 150,
    opacity: 0.5
  },
  badgeWrapper: {
    justifyContent: 'center',
    alignItems: 'center'
  }
});


export default BadgeViewer
