import React, {Component} from 'react';

import {
    StyleSheet,
    View, Text, Image, TouchableOpacity, Dimensions
} from 'react-native';
import {Card, Overlay} from 'react-native-elements';

class BadgeDescription extends Component {
    constructor(props) {
        super(props);

        this.dim = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height
        };
    }

    render() {
        //TODO get date obtention
        let badge = this.props.badge;
        let obtenu = this.props.dateObtention !== null;
        return (
            <Overlay
                isVisible={true}
                onBackdropPress={this.props.onBackdropPress}
                style={styles.overlayWrapper}
                height={this.dim.height - 210}
            >
                <TouchableOpacity style={{flex: 1}} onPress={this.props.onBackdropPress}>
                    <View style={{flex: 1}}>
                        <View style={styles.badgArea}>
                            <View style={styles.badgImg}>
                                <Image
                                    source={{uri: badge.badgImageUrl}}
                                    style={obtenu ? styles.badgeReceived : styles.badgeNotReceived}/>
                                {
                                    obtenu ? null :
                                        <View style={{
                                            width: 150, height: 150,
                                            borderRadius: 100, position: 'absolute',
                                            opacity: 0.5, backgroundColor: '#333'
                                        }}
                                        />
                                }
                            </View>
                            <View style={styles.badgObtain}>
                                <Text style={{fontSize: 24}}>{badge.badgNom}</Text>
                                {
                                    obtenu ?
                                        <Text>Obtenu le {this.props.dateObtention}</Text> :
                                        <Text>Non obtenu</Text>
                                }
                            </View>
                        </View>
                        <View style={styles.badgDetailsArea}>
                            <View style={styles.badgDetails}>
                                <Text style={styles.infoTitle}>Description :</Text>
                                <Text>{badge.badgDescription}</Text>
                            </View>
                            <View style={styles.badgDetails}>
                                <Text style={styles.infoTitle}>Nombre de votes requis :</Text>
                                <Text>{badge.nbPointsRequis} vote{badge.nbPointsRequis > 1 ? 's' : null}</Text>
                            </View>
                            <View style={styles.badgDetails}>
                                <Text style={styles.infoTitle}>Compétence associée :</Text>
                                <Text>{badge.competence.compNom}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Overlay>
        )
            ;
    }

}

const styles = StyleSheet.create({
    badgArea: {
        paddingBottom: 10,
        marginTop: 4
    },
    badgImg: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    badgObtain: {
        alignItems: 'center',
        marginLeft: 10,
        marginBottom: 2
    },
    badgDetailsArea: {
        paddingHorizontal: 6,
        paddingTop: 10,
        borderWidth: 2,
        borderColor: '#ddd'
    },
    badgDetails: {
      marginBottom: 10
    },
    overlayWrapper: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column'
    },
    badgeReceived: {
        width: 150,
        height: 150
    },
    badgeNotReceived: {
        width: 150,
        height: 150,
        opacity: 0.5
    },
    infoTitle: {
        fontWeight: 'bold',
        fontSize: 20
    }
});

export default BadgeDescription
