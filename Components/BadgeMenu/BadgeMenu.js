import React, { Component } from 'react';

// Components
import { StyleSheet, View } from 'react-native';
import Category from 'react-native-category';

// Redux
//import { connect } from 'react-redux';

// Style
import theme from './../../Assets/Styles/Theme';

class BadgeMenu extends Component {
    constructor(props) {
        super(props);
    }

    _itemChoose(item) {
        this.props.handleSelectedCategory(item);
    }

    _displayParentBadges() {
        if (this.props.parentBadges !== null && this.props.parentBadges !== '') {
            if (this.props.parentBadges.length > 0) {
                return (
                    <View>
                      <Category
                        data={this.props.parentBadges}
                        itemSelected={(badge) => this._itemChoose(badge)}
                        itemText={'badgNom'}  //set attribule of object show in item category
                        textType={'capitalize'}
                        colorTextDefault={theme.textCatItemDefault}
                        colorTextSelected={theme.textCatItemSelected}
                        colorItemDefault={theme.bgCatItemDefault}
                        colorItemSelected={theme.bgCatItemSelected}
                        style={theme.bgSecondary}
                        />
                    </View>
                )
            }
        }
    }

    render(){
        return (
            <View style={{flex: 1}}>
                { this._displayParentBadges() }
            </View>
        );
    }

/*    _actionSaveBadgesList() {
        console.log('ACTION _actionSaveBadgesList');
        const action = { type: "SAVE_BADGES_LIST", value: {skills: this.state.badges} };
        this.props.dispatch(action);
    }*/
}

/*
const mapStateToProps = (state) => {
    return {
        token: state.storageLogin.token,
        badges: state.storageApp.badges,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
};
*/


export default BadgeMenu
